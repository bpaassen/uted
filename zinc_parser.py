#!/usr/bin/python3
"""
A simple parser for the smiles subset.

"""
# Copyright (C) 2021
# Benjamin Paaßen
# Humboldt-University of Berlin

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from alkanes_parser import Tree
from alkanes_parser import parse_molecule_

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2021, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@hu-berlin.de'


def parse_zinc(path = 'zinc_subset.all.in'):
    """ Parses the zinc dataset and returns it as
    a list of trees and a list of chemical stability values.

    Parameters
    ----------
    path: string
        Path to the zinc_subset.all.in file.

    Returns
    -------
    trees: list
        A list of trees, each being a tuple of a node list and
        an adjacency list.
    stabilities: list
        A list of stabilities.

    """
    trees = []
    stabilities = []
    with open(path) as f:
        for line in f:
            if line.startswith('#') or line.isspace():
                continue
            # now we should have a line with a molecule on it,
            # so start parsing
            tree, i = parse_molecule_(line, 0)
            # simplify tree somewhat
            tree = simplify_tree_(tree)
            trees.append(tree.to_list_format())
            # extract stability and append it
            j = line[i+1:].index('(')
            stability = float(line[i+1:i+j])
            stabilities.append(stability)
    return trees, stabilities

def simplify_tree_(tree):
    if tree._label in ['branched_atom', 'bracket_atom']:
        atom = simplify_tree_(tree._children[0])
        for k in range(1, len(tree._children)):
            child = simplify_tree_(tree._children[k])
            atom._children.append(child)
        return atom
    if tree._label == 'single_chain':
        atom = simplify_tree_(tree._children[1])
        child = simplify_tree_(tree._children[0])
        atom._children.append(child)
        return atom
    if tree._label in ['double_chain', 'triple_chain', 'quad_chain']:
        atom = simplify_tree_(tree._children[1])
        child = simplify_tree_(tree._children[0])
        chain = Tree(tree._label)
        atom._children.append(chain)
        chain._children.append(child)
        return atom
    if tree._label == 'chain_end':
        return simplify_tree_(tree._children[0])
    # per default, step recursively through the tree
    node = Tree(tree._label)
    for k in range(len(tree._children)):
        node._children.append(simplify_tree_(tree._children[k]))
    return node
