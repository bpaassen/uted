# Unordered Tree Edit Distance

Copyright (C) 2021  
Benjamin Paaßen  
Humboldt-University of Berlin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Introduction

This repository contains the reference implementation of [An A*-algorithm for the Unordered Tree Edit Distance with Custom Costs][Paper]. If you use this code in academic work, please cite

* Paaßen, B. (2021). An A*-algorithm for the Unordered Tree Edit Distance with Custom Costs. Proceedings of the 14th International Conference on Similarity Search and Applications (SISAP 2021). in press. [Link][Paper]

In particular, this repository contains all experimental scripts, model
implementations, data sets, and auxiliary files necessary to run the
experiments.

Note that the unordered tree edit distance is NP-hard and, therefore,
this implementation requires exponential time in the tree size in
the worst case. In cases where trees have more than ~20 nodes, it may
therefore be more advisable to use other kinds of edit distances,
as provided by the [edist][edist] package.

In the remainder of this README, we provide the required packages to run the
modules of this repository, and a detailed list of all enclosed files with explanations.

## Installation Instructions

All software enclosed in this repository is written in Python 3.
It depends on [numpy][numpy] (Version >= 1.20) and [scipy][scipy]  (Version >= 1.6.).
The experimental code further depends on [scikit-learn][sklearn] (Version >= 0.21.3) and [edist][edist] (Version >= 1.1.0).

All dependencies are available on pip.

## Quickstart Guide

To compute the unordered tree edit distance between two trees,
you first need to bring your trees into node list/adjacency list
format. The node list is a list of all nodes in depth-first-search
order and the adjacency list is a list of lists, providing the
indices of children for each node. For example, the trees
x = a(b(c, d), e) and y = a(e, d, c) from the paper would be
represented as follows.

```Python
x_nodes = ['a', 'c', 'c', 'd', 'e']
x_adj   = [[1, 4], [2, 3], [], [], []]
y_nodes = ['a', 'e', 'd', 'c']
y_adj   = [[2, 3, 4], [], [], []]
```

Importantly, note that the indexing of nodes for the adjacency list
begins at zero for the root!

Once this is done, you can just call the `uted.uted_astar` function
to compute the edit distance.

```Python
from uted import uted_astar
d, alignment, N = uted_astar(x_nodes, x_adj, y_nodes, y_adj)
```

Here, `d` is the value of the edit distance (1 in this case),
`alignment` contains the alignment partners for nodes 0, 1, 2, 3,
and 4 from x (`[0, -1, 3, 2, 1]`, in this case), and N is the number
of partial alignments that were considered during the A* computation
(14 in this case).

## Background

The unordered tree edit distance between two trees x and y is defined
as the cost of the cheapest sequence of node deletions, node 
insertions, and node relabelings, such that x becomes y, ignoring the
order of siblings. For example, the unordered tree edit distance
between the trees a(b, c) and a(c, b) is zero because we are allowed
to ignore the different order of b, c and c, b. Another way to frame
this is that the unordered tree edit distance also permits an edit
which swaps the order of neighboring siblings and has zero cost.

While other tree edit distances can be computed in polynomial time
via dynamic programming, the unordered tree edit distance is
NP-hard ([Zhang ang Jiang, 1994][Zhang1994]). As such, we rely on
heuristic approaches, such as the A* algorithm ([Horesh, Mehr, and Unger, 2006][Horesh2006]; [Yoshino, Higuchi, and Hirata, 2013][Yoshino2013]).
In essence, the A* algorithm works by deciding for each node in x
whether it gets deleted or relabeled (i.e. mapped to a node in y)
and adds insertions in between whenever necessary. All possible
decision options are kept and associated with a lower bound for the
possible unordered tree edit distance we can still achieve.
Then, the option with the lowest lower bound is continued until we
have achieved a complete edit sequence. Once that happens, no lower
bound remains that could beat our solution, which means that we
found an optimal edit sequence.

Crucially, the A* algorithm relies on this lower bound. The prior
works of [Horesh et al. (2006)][Horesh2006] and [Yoshino et al. (2013)][Yoshino2013]
only provided lower bounds for the _unit_ edit distance, where all
edits cost the same. In this work, we provide lower bounds that work
for arbitrary edit costs (provided that they are still metric).
We also show that our heuristics are, runtime-wise, competitive with
the current state of the art by [Yoshino et al. (2013)][Yoshino2013].

## Contents

In more detail, the following files are contained in this repository (in
alphabetical order):

* `alkanes-bp.all.in` : The alkanes data set from [Micheli](http://pages.di.unipi.it/micheli/dataset/).
* `alkanes_5nn_prediction.csv` : The 5NN prediction results for the alkanes data set.
* `alkanes_experiment.ipynb` : An Ipython notebook containing all experimental code for the alkanes experiment.
* `alkanes_parser.py` : A Python module to support parsing the alkanes data set.
* `alkanes_rmses.csv` : The prediction RMSES for the alkanes data set.
* `alkanes_runtimes.csv` : The runtime results for the alkanes data set.
* `uted.py` : The reference implementation of our proposed heuristics and A* algorithm.
* `uted_test.py` : A small suite of unit tests.
* `zinc_5nn_prediction.csv` : The 5NN prediction results for the ZINC data set.
* `zinc_experiment.ipynb` : An Ipython notebook containing all experimental code for the ZINC experiment.
* `zinc_parser.py` : A Python module to support parsing the ZINC data set.
* `zinc_rmses.csv` : The prediction RMSES for the ZINC data set.
* `zinc_runtimes.csv` : The runtime results for the ZINC data set.
* `zinc_subset.all.in` : The 100 smallest molecules from the ZINC data set of [Kusner](https://github.com/mkusner/grammarVAE/tree/master/data).

## Literature

* Horesh, Y., Mehr, R., and Unger, R. (2006). Designing an A* Algorithm for Calculating Edit Distance between Rooted-Unordered Trees. Journal of Computational Biology, 13(6), pp. 1165-1176. doi:[10.1089/cmb.2006.13.1165][Horesh2006]
* Paaßen, B. (2021). An A*-algorithm for the Unordered Tree Edit Distance with Custom Costs. Proceedings of the 14th International Conference on Similarity Search and Applications (SISAP 2021). in press. [Link][Paper]
* Yoshino, T., Higuchi, S., and Hirata, K. (2013). A Dynamic Programming A* Algorithm for Computing Unordered Tree Edit Distance. Proceedings of the Second IIAI International Conference on Advanced Applied Informatics, pp. 135-140. doi:[10.1109/IIAI-AAI.2013.71][Yoshino2013]
* Zhang, K., and Jiang, T. (1994). Some MAX SNP-hard results concerning unordered labeled trees. Information Processing Letters, 49(5), pp. 249-254. doi:[10.1016/0020-0190(94)90062-0][Zhang1994]
* Zhang, K. (1996). A constrained edit distance between unordered labeled trees. Algorithmica, 15(3), pp. 205-222. doi:[10.1007/BF01975866][Zhang1996]

[edist]:https://gitlab.ub.uni-bielefeld.de/bpaassen/python-edit-distances "edist homepage."
[numpy]:https://numpy.org/ "numpy homepage."
[pytorch]:https://pytorch.org/ "PyTorch homepage."
[scipy]:https://scipy.org/ "scipy homepage."
[sklearn]:https://scikit-learn.org/stable/ "scikit-learn homepage"
[Paper]:https://arxiv.org/abs/2108.00953 "Paaßen, B. (2021). An A*-algorithm for the Unordered Tree Edit Distance with Custom Costs. Proceedings of the 14th International Conference on Similarity Search and Applications (SISAP 2021). in press."
[Horesh2006]:https://doi.org/10.1089/cmb.2006.13.1165 "Horesh, Y., Mehr, R., and Unger, R. (2006). Designing an A* Algorithm for Calculating Edit Distance between Rooted-Unordered Trees. Journal of Computational Biology, 13(6), pp. 1165-1176. doi:10.1089/cmb.2006.13.1165"
[Yoshino2013]:https://doi.org/10.1109/IIAI-AAI.2013.71 "Yoshino, T., Higuchi, S., and Hirata, K. (2013). A Dynamic Programming A* Algorithm for Computing Unordered Tree Edit Distance. Proceedings of the Second IIAI International Conference on Advanced Applied Informatics, pp. 135-140. doi:10.1109/IIAI-AAI.2013.71"
[Zhang1994]:https://doi.org/10.1016/0020-0190(94)90062-0 "Zhang, K., and Jiang, T. (1994). Some MAX SNP-hard results concerning unordered labeled trees. Information Processing Letters, 49(5), pp. 249-254. doi:10.1016/0020-0190(94)90062-0"
[Zhang1996]:https://doi.org/10.1007/BF01975866 "Zhang, K. (1996). A constrained edit distance between unordered labeled trees. Algorithmica, 15(3), pp. 205-222. doi:10.1007/BF01975866"
