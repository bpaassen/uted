#!/usr/bin/python3
"""
Tests the unordered tree edit distance implementation.

"""
# Copyright (C) 2021
# Benjamin Paaßen
# Humboldt-University of Berlin

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import time
import numpy as np
from scipy.spatial.distance import cdist
import uted

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2021, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@hu-berlin.de'


def string_to_tree(string_rep):
    node, i = string_to_tree_(string_rep, 0)
    return node.to_list_format()

def string_to_tree_(s, i = 0):
    label = s[i]
    node  = Tree(label)
    j = i + 1
    if s[j] == '(':
        while s[j] != ')':
            child, j = string_to_tree_(s, j+1)
            node._children.append(child)
        return node, j+1
    elif s[j] == ',' or s[j] == ')':
        return node, j
    else:
        raise ValueError('unexpected symbol: %s' % str(s[j]))

class TestUTED(unittest.TestCase):

    def test_heuristics(self):
        # we consider an example of two-dimensional points where
        # the cubic heuristic finds the best assignment but the
        # other two heuristics underestimate the cost.
        x_nodes = [[0., 0.], [-1., 0.], [1., 1.], [1., -1.]]
        x_adj   = [[1, 2, 3], [], [], []]
        y_nodes = [[0., 0.], [1., 0.5], [2., 1.]]
        y_adj   = [[1, 2], [], []]

        dels = np.zeros(len(x_nodes))
        for i in range(len(x_nodes)):
            dels[i] = np.linalg.norm(x_nodes[i])
        inss = np.zeros(len(y_nodes))
        for j in range(len(y_nodes)):
            inss[j] = np.linalg.norm(y_nodes[j])
        reps = cdist(x_nodes, y_nodes)

        h = uted.LinearHeuristic(dels, inss, reps)
        d = h.apply(1, len(x_nodes), range(1, len(y_nodes)))
        # according to the linear heuristic, we only need to delete
        # a single node
        self.assertAlmostEqual(dels[1], d)

        h = uted.QuadraticHeuristic(dels, inss, reps)
        d = h.apply(1, len(x_nodes), range(1, len(y_nodes)))
        # according to the quadratic heuristic, we need to
        # delete a single node and can re-use nodes for replacement
        self.assertAlmostEqual(dels[1] + reps[2, 1] + reps[3, 1], d)

        h = uted.CubicHeuristic(dels, inss, reps)
        d = h.apply(1, len(x_nodes), range(1, len(y_nodes)))
        # the cubic heuristic should find the actual valid solution
        self.assertAlmostEqual(dels[1] + reps[2, 2] + reps[3, 1], d)


        # check for symmetry
        h = uted.LinearHeuristic(inss, dels, reps.T)
        d = h.apply(1, len(y_nodes), range(1, len(x_nodes)))
        self.assertAlmostEqual(dels[1], d)

        h = uted.QuadraticHeuristic(inss, dels, reps.T)
        d = h.apply(1, len(y_nodes), range(1, len(x_nodes)))
        self.assertAlmostEqual(dels[1] + reps[2, 1] + reps[3, 1], d)

        h = uted.CubicHeuristic(inss, dels, reps.T)
        d = h.apply(1, len(y_nodes), range(1, len(x_nodes)))
        self.assertAlmostEqual(dels[1] + reps[2, 2] + reps[3, 1], d)

    def test_uted(self):
        # test a trivial example: aligning a single leaf
        x_nodes = ['a']
        x_adj   = [[]]
        y_nodes = ['a', 'c', 'd', 'e', 'f']
        y_adj   = [[1, 4], [2, 3], [], [], []]

        d, alignment, N = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(4., d)
        self.assertEqual([0], alignment)

        # test symmetry
        d, alignment, N = uted.uted_astar(y_nodes, y_adj, x_nodes, x_adj)
        self.assertAlmostEqual(4., d)
        self.assertEqual([0, -1, -1, -1, -1], alignment)

        # test an example with a single free node
        x_nodes = ['a', 'e']
        x_adj   = [[1], []]
        y_nodes = ['a', 'c', 'd', 'e', 'f']
        y_adj   = [[1, 4], [2, 3], [], [], []]

        d, alignment, N = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(3., d)
        self.assertEqual([0, 3], alignment)

        # test symmetry
        d, alignment, N = uted.uted_astar(y_nodes, y_adj, x_nodes, x_adj)
        self.assertAlmostEqual(3., d)
        self.assertEqual([0, -1, -1, 1, -1], alignment)


        # test an example with two full trees
        x_nodes = ['a', 'b', 'c', 'e', 'd']
        x_adj   = [[1], [2], [3, 4], [], []]
        y_nodes = ['a', 'c', 'd', 'e', 'f']
        y_adj   = [[1, 4], [2, 3], [], [], []]

        d, alignment, N = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj)

        self.assertAlmostEqual(2., d)
        self.assertEqual([0, -1, 1, 3, 2], alignment)

        # test symmetry
        d, alignment, N = uted.uted_astar(y_nodes, y_adj, x_nodes, x_adj)
        self.assertAlmostEqual(2., d)
        self.assertEqual([0, 2, 4, 3, -1], alignment)

        # verify that all heuristics yield the same result
        for h in range(1, 4):
            d, alignment, N = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj, heuristic = h)

            self.assertAlmostEqual(2., d)
            self.assertEqual([0, -1, 1, 3, 2], alignment)

        d, alignment, N = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj, heuristic = 'yoshino')

        self.assertAlmostEqual(2., d)
        self.assertEqual([0, -1, 1, 3, 2], alignment)

        # verify that a manually defined delta with unit costs yields the same result
        def delta(x, y):
            if x == y:
                return 0.
            else:
                return 1.

        d, alignment, N = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj, delta = delta)

        self.assertAlmostEqual(2., d)
        self.assertEqual([0, -1, 1, 3, 2], alignment)


    def test_uted_constrained(self):
        # test a trivial example: aligning a single leaf
        x_nodes = ['a']
        x_adj   = [[]]
        y_nodes = ['a', 'c', 'd', 'e', 'f']
        y_adj   = [[1, 4], [2, 3], [], [], []]

        d = uted.uted_constrained(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(4., d)

        # test symmetry
        d = uted.uted_constrained(y_nodes, y_adj, x_nodes, x_adj)
        self.assertAlmostEqual(4., d)

        # test an example with a single free node
        x_nodes = ['a', 'e']
        x_adj   = [[1], []]
        y_nodes = ['a', 'c', 'd', 'e', 'f']
        y_adj   = [[1, 4], [2, 3], [], [], []]

        d = uted.uted_constrained(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(3., d)

        # test symmetry
        d = uted.uted_constrained(y_nodes, y_adj, x_nodes, x_adj)
        self.assertAlmostEqual(3., d)


        # test an example with two full trees
        x_nodes = ['a', 'b', 'c', 'e', 'd']
        x_adj   = [[1], [2], [3, 4], [], []]
        y_nodes = ['a', 'c', 'd', 'e', 'f']
        y_adj   = [[1, 4], [2, 3], [], [], []]

        d = uted.uted_constrained(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(2., d)

        # test symmetry
        d = uted.uted_constrained(y_nodes, y_adj, x_nodes, x_adj)
        self.assertAlmostEqual(2., d)



    def test_paper_example(self):
        # test the example trees from the paper which should explain
        # the difference of the full UTED and the constrained UTED

        x_nodes = ['a', 'b', 'c', 'd', 'e']
        x_adj   = [[1, 4], [2, 3], [], [], []]
        y_nodes = ['a', 'e', 'd', 'c']
        y_adj   = [[1, 2, 3], [], [], []]

        d, alignment, _ = uted.uted_astar(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(1., d)
        self.assertEqual([0, -1, 3, 2, 1], alignment)

        d = uted.uted_constrained(x_nodes, x_adj, y_nodes, y_adj)
        self.assertAlmostEqual(3., d)

if __name__ == '__main__':
    unittest.main()
