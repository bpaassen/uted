#!/usr/bin/python3
"""
A simple parser for the alkanes dataset from
http://pages.di.unipi.it/micheli/dataset/

"""
# Copyright (C) 2021
# Benjamin Paaßen
# Humboldt-University of Berlin

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2021, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@hu-berlin.de'


class Tree:
    """ Models a tree as a recursive data structure with a label and
    children.

    Attributes
    ----------
    _label: str
        The label of this tree.
    _children: list
        The children of this tree. Should be a list of trees.

    """
    def __init__(self, label, children = None):
        self._label = label
        if(children is None):
            self._children = []
        else:
            self._children = children

    def __str__(self):
        out = str(self._label)
        if(not self._children):
            return out
        else:
            child_strs = [str(child) for child in self._children]
            return out + '(%s)' % ', '.join(child_strs)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if isinstance(other, Tree):
            return self._label == other._label and self._children == other._children
        return False

    def to_list_format(self):
        """ Convers this tree to node list/adjacency list format via depth
        first search.

        Returns
        -------
        nodes: list
            the node list.
        adj: list
            the adjacency list.

        """
        # initialize node and adjacency list
        nodes = []
        adj   = []
        # perform the depth first search via a stack which stores
        # the parent index and the current tree
        stk = [(-1, self)]
        while(stk):
            p, tree = stk.pop()
            i = len(nodes)
            # append the label to the node list
            nodes.append(tree._label)
            # append a new empty child list
            adj.append([])
            # append the current node to its parent
            if(p >= 0):
                adj[p].append(i)
            # push the children onto the stack
            for c in range(len(tree._children)-1, -1, -1):
                if(tree._children[c] is None):
                    continue
                if(isinstance(tree._children[c], list)):
                    for c2 in range(len(tree._children[c]) -1, -1, -1):
                        stk.append((i, tree._children[c][c2]))
                    continue
                stk.append((i, tree._children[c]))
        return nodes, adj



def parse_alkanes(path = 'alkanes-bp.all.in'):
    """ Parses the alkanes dataset and returns it as
    a list of trees and a list of boiling point temperatures.

    Parameters
    ----------
    path: string
        Path to the alkanes-bp.all.in file.

    Returns
    -------
    trees: list
        A list of trees, each being a tuple of a node list and
        an adjacency list.
    temperatures: list
        A list of temperatures.

    """
    trees = []
    temperatures = []
    with open(path) as f:
        for line in f:
            if line.startswith('#') or line.isspace():
                continue
            # now we should have a line with a molecule on it,
            # so start parsing
            tree, i = parse_molecule_(line, 0)
            # append the tree to the list
            trees.append(tree.to_list_format())
            # extract temperature and append it
            j = line[i+1:].index('(')
            temperature = float(line[i+1:i+j])
            temperatures.append(temperature)
    return trees, temperatures

def parse_molecule_(line, i):
    # parse the current token
    j = i
    while line[j] not in ['(', ')', ',', '.']:
        j += 1
    token = line[i:j]
    # generate a new tree node
    node = Tree(token)
    # treat the next control symbol. If it is
    # an open bracket, we need to go deeper into recursion
    # until we hit a closing bracket
    if line[j] == '(':
        while line[j] != ')':
            # parse the next child and append it
            child, j = parse_molecule_(line, j+1)
            node._children.append(child)
        # go beyond the closing bracket
        j += 1
    # return
    return node, j
